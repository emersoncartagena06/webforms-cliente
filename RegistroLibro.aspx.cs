﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClienteWebF
{
    public partial class RegistroLibro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string id = Request.QueryString["id"];
                txID.Value = id;

                listarAutores();
                listarEditoriales();
                listarGeneros();

                if (id == null)
                {
                    lbCabecera.Text = "Agregar Libro";
                }
                else
                {
                    lbCabecera.Text = "Editar Libro";
                    txCodigo.Text = id;
                    txCodigo.ReadOnly = true;
                    buscarLibro();
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string codigo = txCodigo.Text;
                string nombre = txNombre.Text;
                int existencia = int.Parse(txExistencia.Text.ToString());
                decimal precio = decimal.Parse(txPrecio.Text);
                string codigoAutor = ddlAutor.SelectedValue.ToString();
                string codigoEditorial = ddlEditorial.SelectedValue.ToString();
                int idGenero = int.Parse(ddlGenero.SelectedValue.ToString());
                string descripcion = txDescripcion.Text;

                LibroService.LibroServiceSoapClient service = new LibroService.LibroServiceSoapClient();
                bool response = false;

                if (txID.Value == "")
                {
                    response = service.AgregarLibro(codigo, nombre, existencia, precio, codigoAutor, codigoEditorial, idGenero, descripcion);
                }
                else
                {
                    response = service.ModificarLibro(codigo, nombre, existencia, precio, codigoAutor, codigoEditorial, idGenero, descripcion);
                }

                if (response)
                {
                    Response.Redirect("~/Libro.aspx");
                }
                else
                {
                  //  MessageBox.Show("Hubo un error");
                }

            }
            catch (Exception ex)
            {
                //MessageBox.Show("Hubo un error");
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Libro.aspx");
        }


        public void listarAutores()
        {
            try
            {
                AutorService.AutorServiceSoapClient service = new AutorService.AutorServiceSoapClient();
                AutorService.Item[] listaWS = service.ListarCombo();

                ddlAutor.DataSource = listaWS;
                ddlAutor.DataTextField = "nombre";
                ddlAutor.DataValueField = "codigo";
                ddlAutor.DataBind();
            }
            catch (Exception ex)
            {
               /* MessageBox.Show("No se pudo cargar los autores");
                this.DialogResult = DialogResult.Cancel;
                this.Close();*/
            }
        }

        public void listarEditoriales()
        {
            try
            {
                EditorialService.EditorialServiceSoapClient service = new EditorialService.EditorialServiceSoapClient();
                EditorialService.Item[] listaWS = service.ListarCombo();

                ddlEditorial.DataSource = listaWS;
                ddlEditorial.DataTextField = "nombre";
                ddlEditorial.DataValueField = "codigo";
                ddlEditorial.DataBind();
            }
            catch (Exception ex)
            {
                /*MessageBox.Show("No se pudo cargar las editoriales");
                this.DialogResult = DialogResult.Cancel;
                this.Close();*/
            }
        }

        public void listarGeneros()
        {
            try
            {
                GeneroService.GeneroServiceSoapClient service = new GeneroService.GeneroServiceSoapClient();
                GeneroService.Item[] listaWS = service.ListarCombo();

                ddlGenero.DataSource = listaWS;
                ddlGenero.DataTextField = "nombre";
                ddlGenero.DataValueField = "codigo";
                ddlGenero.DataBind();
            }
            catch (Exception ex)
            {
                /*MessageBox.Show("No se pudo cargar los generos");
                this.DialogResult = DialogResult.Cancel;
                this.Close();*/
            }
        }

        public void buscarLibro()
        {
            try
            {
                LibroService.LibroServiceSoapClient service = new LibroService.LibroServiceSoapClient();
                LibroService.Libro libroWS = service.BuscarLibro(txID.Value);
                if (libroWS != null)
                {
                    txNombre.Text = libroWS.nombreLibro;
                    txExistencia.Text = libroWS.existencia.ToString();
                    txPrecio.Text = libroWS.precio.ToString();
                    ddlAutor.SelectedValue = libroWS.autor.codigo;
                    ddlEditorial.SelectedValue = libroWS.editorial.codigo;
                    ddlGenero.SelectedValue = libroWS.genero.id.ToString();
                    txDescripcion.Text = libroWS.descripcion;
                }
                else
                {
                   /* MessageBox.Show("No se pudo encontrar el libro");
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();*/
                }
            }
            catch (Exception ex)
            {
                /*MessageBox.Show("No se pudo encontrar el libro");
                this.DialogResult = DialogResult.Cancel;
                this.Close();*/
            }
        }

       
    }
}