﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="RegistroLibro.aspx.cs" Inherits="ClienteWebF.RegistroLibro" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

   <div class="card">
       <div class="card-header">
           <h4><asp:label ID="lbCabecera" runat="server" /></h4>
           <asp:HiddenField ID="txID" runat="server" />
       </div>
       <div class="card-body">
           <div class="row">
               <label class="control-label col-md-3">Código</label>
               <div class="col-md-9">
                   <asp:TextBox runat="server" ID="txCodigo" CssClass="form-control" />
               </div>
           </div>
           <div class="row">
               <label class="control-label col-md-3">Nombre</label>
               <div class="col-md-9">
                   <asp:TextBox runat="server" ID="txNombre" CssClass="form-control" />
               </div>
           </div>
           <div class="row">
               <label class="control-label col-md-3">Existencia</label>
               <div class="col-md-9">
                   <asp:TextBox runat="server" ID="txExistencia" CssClass="form-control" />
               </div>
            </div>
           <div class="row">
               <label class="control-label col-md-3">Precio</label>
               <div class="col-md-9">
                   <asp:TextBox runat="server" ID="txPrecio" CssClass="form-control" />
               </div>               
           </div>

           <div class="row">
               <label class="control-label col-md-3">Autor</label>
               <div class="col-md-9">
                   <asp:DropDownList runat="server" ID="ddlAutor" CssClass="form-control" />
               </div>               
           </div>

            <div class="row">
               <label class="control-label col-md-3">Editorial</label>
               <div class="col-md-9">
                   <asp:DropDownList runat="server" ID="ddlEditorial" CssClass="form-control" />
               </div>               
           </div>

           <div class="row">
               <label class="control-label col-md-3">Genero</label>
               <div class="col-md-9">
                   <asp:DropDownList runat="server" ID="ddlGenero" CssClass="form-control" />
               </div>               
           </div>

           <div class="row">
               <label class="control-label col-md-3">Descripción</label>
               <div class="col-md-9">
                   <asp:TextBox runat="server" ID="txDescripcion" CssClass="form-control" />
               </div>               
           </div>


       </div>

       <div class="card-footer">
           <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-success" OnClick="btnGuardar_Click" />
           <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-danger" OnClick="btnCancelar_Click" />
       </div>
   </div>

</asp:Content>
