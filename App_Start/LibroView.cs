﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClienteWebF.App_Start
{
    public class LibroView
    {
        public string codigoLibro { get; set; }
        public string nombreLibro { get; set; }
        public int existencia { get; set; }
        public decimal precio { get; set; }
        public string autor { get; set; }
        public string editorial { get; set; }
        public string genero { get; set; }
        public string descripcion { get; set; }
    }
}