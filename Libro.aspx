﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="Libro.aspx.cs" Inherits="ClienteWebF.Libro" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Libros</h1>
    
    <asp:Button ID="btnAgregar" runat="server"  Text ="Agregar" OnClick="btnAgregar_Click"/>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-hover" OnRowEditing="GridView1_RowEditing">
        <Columns>
            <asp:BoundField DataField="codigoLibro" HeaderText="Código" />
            <asp:BoundField DataField="nombreLibro" HeaderText="Libro" />
            <asp:BoundField DataField="existencia" HeaderText="Existencia" />
            <asp:BoundField DataField="precio" HeaderText="Precio" />
            <asp:BoundField DataField="autor" HeaderText="Autor" />
            <asp:BoundField DataField="editorial" HeaderText="Editorial" />
            <asp:BoundField DataField="Genero" HeaderText="genero" />
            <asp:BoundField DataField="descripcion" HeaderText="Descripción" />
            <asp:CommandField ShowCancelButton="False" ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
    </asp:GridView>

</asp:Content>

