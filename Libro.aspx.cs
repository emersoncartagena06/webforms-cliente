﻿using ClienteWebF.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ClienteWebF
{
    public partial class Libro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                listar();
            }            
        }

        public void listar()
        {
            List<LibroView> lista = new List<LibroView>();
            try
            {
                LibroService.LibroServiceSoapClient service = new LibroService.LibroServiceSoapClient();
                LibroService.Libro[] listaWS = service.ListarLibros();

                foreach (LibroService.Libro item in listaWS)
                {
                    lista.Add(new LibroView()
                    {
                        codigoLibro = item.codigoLibro,
                        nombreLibro = item.nombreLibro,
                        existencia = item.existencia,
                        precio = item.precio,
                        autor = item.autor.nombre,
                        editorial = item.editorial.nombre,
                        genero = item.genero.nombre,
                        descripcion = item.descripcion
                    });
                }

            }
            catch (Exception ex)
            {
                lista = new List<LibroView>();
            }
            GridView1.DataSource = lista;
            GridView1.DataBind();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/RegistroLibro.aspx");
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            string codigo = GridView1.Rows[e.NewEditIndex].Cells[0].Text;
            Response.Redirect("~/RegistroLibro.aspx?id="+codigo);
        }
    }
}